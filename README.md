# NAME

CXC::Number - A namespace for modules which deal with numbers.

# VERSION

version 0.14

# DESCRIPTION

The following are known:

- [CXC::Number::Grid](https://metacpan.org/pod/CXC%3A%3ANumber%3A%3AGrid)

    A representation of a grid of numbers, with the ability to join and
    overlay grids.  Useful for binning data.

- [CXC::Number::Sequence](https://metacpan.org/pod/CXC%3A%3ANumber%3A%3ASequence)

    A namespace and module for dealing with sequences of numbers, often
    constructed so that they are useful for binning data.

# INTERNALS

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-cxc-number@rt.cpan.org  or through the web interface at: [https://rt.cpan.org/Public/Dist/Display.html?Name=CXC-Number](https://rt.cpan.org/Public/Dist/Display.html?Name=CXC-Number)

## Source

Source is available at

    https://gitlab.com/djerius/cxc-number

and may be cloned from

    https://gitlab.com/djerius/cxc-number.git

# SEE ALSO

Please see those modules/websites for more information related to this module.

- [CXC::Number::Grid](https://metacpan.org/pod/CXC%3A%3ANumber%3A%3AGrid)
- [CXC::Number::Sequence](https://metacpan.org/pod/CXC%3A%3ANumber%3A%3ASequence)

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2019 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
