package CXC::Number;

# ABSTRACT:  A namespace for modules which deal with numbers.

use v5.28;
use strict;
use warnings;

our $VERSION = '0.14';

# COPYRIGHT

1;

__END__


=head1 DESCRIPTION

The following are known:

=over

=item L<CXC::Number::Grid>

A representation of a grid of numbers, with the ability to join and
overlay grids.  Useful for binning data.

=item L<CXC::Number::Sequence>

A namespace and module for dealing with sequences of numbers, often
constructed so that they are useful for binning data.

=back

=head1 SEE ALSO

CXC::Number::Grid

CXC::Number::Sequence


