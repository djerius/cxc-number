package CXC::Number::Grid::Failure;

# ABSTRACT: CXC::Number::Sequence Exceptions
use v5.28;
use strict;
use warnings;
use experimental 'signatures';

use parent 'Exporter::Tiny';
use custom::failures ();

our $VERSION = '0.14';

our @EXPORT_OK;
BEGIN {
    my @failures = qw/ parameter::constraint
      parameter::unknown
      parameter::interface
      parameter::IllegalCombination
      internal
      /;
    custom::failures->import( __PACKAGE__, @failures );
    @EXPORT_OK = map { s/::/_/r } @failures;
}

sub _exporter_expand_sub ( $, $name, $, $, $ ) {
    my $failure = __PACKAGE__ . q{::} . ( $name =~ s/_/::/gr );
    ## no critic (BuiltinFunctions::ProhibitStringyEval)
    ## no critic (ErrorHandling::RequireCheckingReturnValueOfEval)
    $name => eval "sub () { '$failure' }";
}

1;
