package CXC::Number::Grid::Range;

# ABSTRACT: Helper class to track Ranges

use v5.28;

use Moo;
use experimental 'signatures';

use namespace::clean;

our $VERSION = '0.14';

use overload
  fallback => 0,
  bool     => sub { 1 },
  '""'     => \&to_string,
  '.'      => \&concatenate;

=attr layer

The grid layer id

=cut

has layer => ( is => 'ro' );

=attr include

Whether this range is included or excluded.

=cut

has include => ( is => 'ro' );

=attr lb

The inclusive range lower bound

=cut

has lb => ( is => 'ro' );

=attr ub

The exclusive range upper bound

=cut

has ub => ( is => 'ro' );

=for Pod::Coverage
BUILDARGS

=cut

around BUILDARGS => sub ( $orig, $class, @args ) {

    my %args = ref $args[0] ? $args[0]->%* : @args;

    @args{ 'layer', 'include' } = delete( $args{value} )->@*
      if defined $args{value};

    return $class->$orig( \%args );
};

=overload ""

Stringification is overloaded via the L</to_string> method.

=cut

=method to_string

  $string = $self->to_string

Return a string representation of the range.

=cut

sub to_string ( $self, $ =, $ = ) {
    my $ub      = $self->ub      // 'undef';
    my $lb      = $self->lb      // 'undef';
    my $layer   = $self->layer   // 'undef';
    my $include = $self->include // 'undef';
    "( $lb, $ub ) => { layer => $layer, include => $include }";
}

=overload .

Concatenation is overloaded via the L</concatenate> method.

=method concatenate

  $string = $range->concatenate( $thing, $swap=false )

Concatenate the stringified version of $range with $thing.

Set C<$swap> to true if the order should be reversed.

=cut

sub concatenate ( $self, $other, $swap = 0 ) {
    my $str = $self->to_string;
    return $swap ? $other . $str : $str . $other;
}

=method value

  [ $layer, $include ] = $range->value;

Return an arrayref containing the layer id and the include value for
the range.

=cut

sub value ( $self ) {
    return [ $self->layer, $self->include ];
}

1;

# COPYRIGHT

__END__

=pod

=for stopwords
ub

=head1 DESCRIPTION

A utility class to manage Ranges when doing bin manipulations with trees.

=cut
