package CXC::Number::Types;

# ABSTRACT: Type::Tiny types for CXC::Number

use v5.28;
use strict;
use warnings;

our $VERSION = '0.14';

use Math::BigInt upgrade => 'Math::BigFloat';
use Math::BigFloat;
use Type::Utils;
use Types::Standard        qw[ Num Int InstanceOf  ];
use Types::Common::Numeric qw[ PositiveNum PositiveOrZeroNum PositiveInt ];

use Type::Library -base, -declare => qw(
  BigInt
  BigNum
  BigPositiveInt
  BigPositiveNum
  BigPositiveOrZeroNum
);

=type BigNum

=cut

class_type BigNum,
  {
    class => 'Math::BigFloat',
    message { 'Not a number or a Math::BigFloat' },
  };

coerce BigNum, from Num, via {
    my $bignum = Math::BigFloat->new( $_ );
    $bignum->is_nan ? $_ : $bignum;
};

=type BigPositiveNum

=cut

declare BigPositiveNum, as BigNum,
  where { $_ > 0 },
  message { BigNum->validate( $_ ) or "$_ is not greater than zero" },
  coercion => 1;

coerce BigPositiveNum, from PositiveNum, via { Math::BigFloat->new( $_ ) };

=type BigPositiveZeroNum

=cut

declare BigPositiveOrZeroNum, as BigNum, where { $_ >= 0 }, message {
    BigNum->validate( $_ )
      or "$_ is not greater than or equal to zero"
}, coercion => 1;

=type BigPositiveOrZeroNum

=cut

coerce BigPositiveOrZeroNum, from PositiveOrZeroNum, via { Math::BigFloat->new( $_ ) };

=type BigInt

=cut

declare BigInt, as( InstanceOf ['Math::BigInt'] | InstanceOf ['Math::BigFloat'] ),
  where { $_->is_int() }, message {
    'Not an integer or a Math::BigInt'
  };

coerce BigInt, from Int, via {
    Math::BigInt->new( $_ );
};

=type BigPositiveInt

A BigInt > 0.

=cut

declare BigPositiveInt, as BigInt,
  where { $_ > 0 },
  message { BigInt->validate( $_ ) or "$_ is not greater than zero" },
  coercion => 1;

coerce BigPositiveInt, from PositiveInt, via { Math::BigFloat->new( $_ ) };


# COPYRIGHT

1;


__END__

=pod

=for stopwords
BigInt
BigNum
BigPositiveInt
BigPositiveNum
BigPositiveOrZeroNum
BigPositiveZeroNum
