package CXC::Number::Sequence::Types;

# ABSTRACT: Type::Tiny types for CXC::Number::Sequence

use v5.28;
use strict;
use warnings;

our $VERSION = '0.14';

use Math::BigInt upgrade => 'Math::BigFloat';
use Math::BigFloat;
use Type::Utils -all;
use Types::Standard        qw[ Num Int Enum Tuple Any InstanceOf Dict ArrayRef Value ];
use Types::Common::Numeric qw[ PositiveNum PositiveOrZeroNum PositiveInt ];

use Type::Library -base, -declare => qw(
  Alignment
  Sequence
  Spacing
  Ratio
);

BEGIN { extends 'CXC::Number::Types' }

=for stopwords
bignum

=cut

=type Alignment

A Tuple containing two elements, the first of which is a C<BigNum>,
the second of which is a C<BigNum> in the range [0,1).

=cut

declare Alignment, as Tuple [ BigNum, BigPositiveOrZeroNum ], where { $_->[1] < 1 }, coercion => 1;

coerce Alignment, from Num, via { [ Math::BigFloat->new( $_ ), Math::BigFloat->new( 0.5 ) ] };


=type Sequence

A array of numbers with at lest two members which is sorted by increasing value

=cut

declare Sequence, as ArrayRef [ BigNum, 2 ], where {
    my $arr = $_;
    $arr->[$_] < $arr->[ $_ + 1 ] || return for 0 .. ( $arr->@* - 2 );
    1;
}, message {
    ArrayRef( [ BigNum, 2 ] )->validate( $_ )
      or 'Must be an array of monotonically increasing numbers with at lest two elements'
}, coercion => 1;

=type Spacing

A non-zero BigNum

=cut


declare Spacing, as BigNum,
  where { $_ != 0 },
  message { BigNum->validate( $_ ) or 'Must be a non-zero number' },
  coercion => 1;


=type Ratio

A positive number greater than 1.

=cut

declare Ratio, as BigNum,
  where { $_ > 0 && $_ != 1 },
  message { BigNum->validate( $_ ) or 'Number must be > 0 && != 1' },
  coercion => 1;

# COPYRIGHT

1;


__END__

