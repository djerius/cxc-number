package CXC::Number::Sequence::Fixed;

# ABSTRACT: CXC::Number::Sequence with arbitrary values

use v5.28;

use Moo;

our $VERSION = '0.14';

use namespace::clean;

extends 'CXC::Number::Sequence';

has '+_raw_elements' => ( required => 1, );


1;

# COPYRIGHT

__END__

=head1 SYNOPSIS

  use CXC::Number::Sequence::Fixed;

  $sequence = CXC::Number::Sequence::Fixed->new( edges => \@edges, %options );

=head1 DESCRIPTION

This subclass of L<CXC::Number::Sequence> specifies the exact elements in a sequence.

