package CXC::Number::Sequence::Role::PDL;

# ABSTRACT: Role to return PDL objects from Sequences

use v5.28;
use PDL::Lite ();

use Moo::Role;

use experimental 'signatures';
use namespace::clean;

our $VERSION = '0.14';

sub _convert ( $self, $bignum ) {
    require Ref::Util;

    return Ref::Util::is_plain_arrayref( $bignum )
      ? PDL->pdl( [ map { $_->numify } $bignum->@* ] )
      : $bignum->numify;
}

# COPYRIGHT

1;

=head1 SYNOPSIS

   my $obj = CXC::Number::Sequence->build( $class, %options)->elements => \@elements );
   Moo::Role->apply_role_to_object( $obj, 'CXC::Number::Sequence::Role::PDL' );

=head1 DESCRIPTION

A L<Moo> role providing a C<_convert> method which returns passed
L<Math::BigFloat> arrays as piddles and passed L<Math::BigFloat> scalars as Perl numbers.

=head1 SEE ALSO

CXC::Number::Sequence

=cut
