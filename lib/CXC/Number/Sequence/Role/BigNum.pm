package CXC::Number::Sequence::Role::BigNum;

# ABSTRACT: Role to return Math::BigFloats from Sequences

use v5.28;

use Moo::Role;

our $VERSION = '0.14';

use experimental 'signatures';
use namespace::clean;

sub _convert ( $self, $bignum ) {
    require Ref::Util;

    return Ref::Util::is_plain_arrayref( $bignum )
      ? [ map { $_->copy } $bignum->@* ]
      : $bignum->copy;
}

# COPYRIGHT

1;

__END__

=head1 SYNOPSIS

   my $obj = CXC::Number::Sequence->build( $class, %options)->elements => \@elements );
   Moo::Role->apply_role_to_object( $obj, 'CXC::Number::Sequence::Role::BigNum' );

=head1 DESCRIPTION

A L<Moo> role providing a C<_convert> method which returns copies of the
passed L<Math::BigFloat> arrays and scalars.

=head1 SEE ALSO

CXC::Number::Sequence
